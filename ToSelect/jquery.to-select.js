(function ($, window, document, undefined) {

	var pluginName = 'toSelect';

	// These settings can be overridden by the user
	var defaultSettings = {

		// Whether or not to use the placeholder text
		usePlaceholder: true,

		// The placeholder text to be displayed when no option
		// is selected and the select list is open
		placeholder: 'No Selection',

		// The element that will wrap the entire select
		wrapper: '<div>',

		// The element that will wrap the selection text display (not the actual option)
		selection: '<span>',

		// The following classes will be added to the elements (uses the BEM class naming convention)
		wrapperClass: 'select',
		openClass: 'select--open',
		optionsClass: 'select__options',
		optionClass: 'select__option',
		selectedOptionClass: 'select__option--selected',
		selectionClass: 'select__selection',
	};

	var Select = {

		init: function (settings, $element) {

			// Overwrite defaults with the settings that are provided
			this.settings = $.extend({}, defaultSettings, settings);

			this.$element = $element;
			this.$options = $element.children();

			this.$selectedOption = null;

			this.buildDOM();
			this.bindEvents();

		},

		/**
		 * Build the DOM elements required to simulate a select.
		 */
		buildDOM: function () {

			// Build the wrapper element
			this.$wrapper = $(this.settings.wrapper);
			this.$wrapper.addClass(this.settings.wrapperClass);

			// Build the selection element
			this.$selection = $(this.settings.selection);
			this.$selection.addClass(this.settings.selectionClass);

			if (this.settings.usePlaceholder) {
				this.$selection.text(this.settings.placeholder);
			} else {
				this.setSelectedOption(0);
			}

			// Add a class to the main element so that we can target it via CSS
			this.$element.addClass(this.settings.optionsClass);

			// Add a class to each option
			this.$options.addClass(this.settings.optionClass);

			// Put it all together
			this.$wrapper = this.$element.wrap(this.$wrapper).parent();
			this.$element.before(this.$selection);

		},

		bindEvents: function () {

			this.$wrapper.on('click', this.toggle.bind(this));
			this.$options.on('click', this.setSelectedOption.bind(this));
			$(document).on('click', this.onBlur.bind(this));

		},

		/**
		 * This function solely deals with modifying the dom's contents.
		 */
		render: function () {

			// If we are using a placeholder and the select is open,
			// we will set the selection text to the placeholder text so
			// that the user knows what options they are selecting from.
			// It is done this way for selects that have no label to inform
			// the user what the select is for.
			if ((this.$selectedOption === null || this.isOpen()) && this.settings.usePlaceholder) {
				this.$selection.text(this.settings.placeholder);
			} else {
				this.$selection.text(this.$selectedOption.text());
			}

		},

		/**
		 * Toggles the visibility of the option list.
		 */
		toggle: function () {

			this.$wrapper.toggleClass(this.settings.openClass);

			this.render();

		},

		/**
		 * Hides the option list.
		 */
		close: function () {

			this.$wrapper.removeClass(this.settings.openClass);

			this.render();

		},

		/**
		 * Checks if the select is open.
		 * @return {Boolean} Whether or not the select is open
		 */
		isOpen: function () {

			return this.$wrapper.hasClass(this.settings.openClass);

		},

		/**
		 * Sets the selected option to the specified index (zero-based). Can also
		 * accept an Event object where the target is the option dom element to
		 * be selected.
		 * @param {Integer or Event} index
		 */
		setSelectedOption: function (index) {

			// Check it 'index' is an event object
			var i = (typeof index === 'object') ? $(index.target).index() : index;

			this.$selectedOption = this.$options.eq(i);

			this.$options.removeClass(this.settings.selectedOptionClass);
			this.$selectedOption.addClass(this.settings.selectedOptionClass);

			this.render();

		},

		/**
		 * This function executes when the anywhere outside of the select
		 * is clicked.
		 * @param  {Event} event The click event object
		 */
		onBlur: function (event) {

			// Check if the clicked element is the select wrapper element
			var isSelect = event.target == this.$wrapper.get(0);

			// Check if the clicked element is any of the elements within the
			// select wrapper (ie. the option elements)
			var isOption = $.contains(this.$wrapper.get(0), event.target);

			// If the user clicked anywhere other than
			// the select, we will close the option list
			if (!isSelect && !isOption) {
				this.close();
			}

		}

	};

	$.fn[pluginName] = function (settings) {

		return this.each(function () {

			var select = Object.create(Select);

			select.init(settings, $(this));

		});

	}

}(jQuery, window, document));